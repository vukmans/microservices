package com.ikea.inter.financeservice.service;

import com.ikea.inter.financeservice.model.Salary;

public interface SalaryService {

    public Integer getSalary(String level, Integer time);
    public Salary testSalary(Long id);
}
