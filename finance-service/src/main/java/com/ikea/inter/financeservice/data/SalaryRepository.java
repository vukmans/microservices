package com.ikea.inter.financeservice.data;

import com.ikea.inter.financeservice.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;


@RepositoryRestResource(path = "salary")
public interface SalaryRepository extends JpaRepository<Salary, Long> {
    Salary findByLevelAndTime(String level, Integer time);

}
