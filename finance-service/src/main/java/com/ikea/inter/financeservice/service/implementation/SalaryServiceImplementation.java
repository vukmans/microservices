package com.ikea.inter.financeservice.service.implementation;

import com.ikea.inter.financeservice.data.SalaryRepository;
import com.ikea.inter.financeservice.model.Salary;
import com.ikea.inter.financeservice.service.SalaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class SalaryServiceImplementation implements SalaryService {

    private final SalaryRepository salaryRepository;

    @Autowired
    public SalaryServiceImplementation(SalaryRepository salaryRepository){
        this.salaryRepository=salaryRepository;
    }

    @Override
    public Integer getSalary(String level, Integer time){
        log.info("Input-->  Level: {}, Time: {}", level, time);
        Salary salary = salaryRepository.findByLevelAndTime(level, time);
        log.info("Output--> Id: {}, Level: {}, Time: {], Value: {}", salary.getId(), salary.getLevel(), salary.getTime(), salary.getValue());
        return salary.getValue();
    }

    @Override
    public Salary testSalary(Long id){
        Optional<Salary> salary = salaryRepository.findById(id);
        return salary.get();
    }
}
