package com.ikea.inter.financeservice.controller;

import com.ikea.inter.financeservice.data.SalaryRepository;
import com.ikea.inter.financeservice.model.Salary;
import com.ikea.inter.financeservice.service.SalaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

//@Slf4j
@RestController
public class SalaryController {

    private final SalaryService salaryService;

    @Autowired
    public SalaryController(SalaryService salaryService){
        this.salaryService = salaryService;
    }

    @GetMapping("/salaries")
    public Integer returnSalary(@RequestParam("level") String level, @RequestParam("time") Integer time){
        return salaryService.getSalary(level, time);
    }

    @GetMapping(name="/testsalary/{id}", produces = APPLICATION_JSON_VALUE)
    public Salary testSalary(@RequestParam("id") Long id){
        //log.info("Returning salary...");
        return salaryService.testSalary(id);
    }
}
