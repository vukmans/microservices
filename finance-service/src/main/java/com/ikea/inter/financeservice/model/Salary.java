package com.ikea.inter.financeservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Salary {

    @Id
    @Column(name = "salary_id")
    private Long id;

    @Column(name = "salary_level")
    private String level;

    @Column(name = "salary_time")
    private Integer time;

    @Column(name = "salary_value")
    private Integer value;
}
