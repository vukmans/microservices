package com.ikea.inter.financeservice.controller;

import com.ikea.inter.financeservice.service.SalaryService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest
public class SalaryControllerTest {

    @MockBean
    SalaryService salaryService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void returnSalary_test() throws Exception {

        Mockito.when(salaryService.getSalary("Junior", 1)).thenReturn(100);
        Mockito.when(salaryService.getSalary("Mid", 3)).thenReturn(500);
        Mockito.when(salaryService.getSalary("Senior", 2)).thenReturn(700);

        mockMvc.perform(MockMvcRequestBuilders.get("/salaries?level=Junior&time=1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string("100"));

        mockMvc.perform(MockMvcRequestBuilders.get("/salaries?level=Mid&time=3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string("500"));

        mockMvc.perform(MockMvcRequestBuilders.get("/salaries?level=Senior&time=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.content().string("700"));
    }
}
