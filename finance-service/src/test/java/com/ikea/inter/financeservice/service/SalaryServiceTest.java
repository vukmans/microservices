package com.ikea.inter.financeservice.service;


import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class SalaryServiceTest {

    @Autowired
    private SalaryService salaryService;

    @Test
    void getSalary_test(){
        Assert.assertEquals((int)salaryService.getSalary("Junior", 1), 100);
        Assert.assertEquals((int)salaryService.getSalary("Junior", 2), 200);

        Assert.assertEquals((int)salaryService.getSalary("Mid", 1), 300);
        Assert.assertEquals((int)salaryService.getSalary("Mid", 2), 400);
        Assert.assertEquals((int)salaryService.getSalary("Mid", 3), 500);

        Assert.assertEquals((int)salaryService.getSalary("Senior", 1), 600);
        Assert.assertEquals((int)salaryService.getSalary("Senior", 2), 700);

        Assert.assertEquals((int)salaryService.getSalary("Test", 10), 0);

    }
}
