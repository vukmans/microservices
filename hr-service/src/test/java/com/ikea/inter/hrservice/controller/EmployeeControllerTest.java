package com.ikea.inter.hrservice.controller;


import com.ikea.inter.hrservice.model.Employee;
import com.ikea.inter.hrservice.service.EmployeeService;
import com.ikea.inter.hrservice.service.FinanceServiceClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @MockBean
    EmployeeService employeeService;

    @MockBean
    FinanceServiceClient financeServiceClient;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void calculateSalary_test() throws Exception {
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Test", 1, "Junior"));
        employeeList.add(new Employee("Test", 2, "Junior"));
        employeeList.add(new Employee("Test", 1, "Mid"));
        employeeList.add(new Employee("Test", 2, "Mid"));
        employeeList.add(new Employee("Test", 3, "Mid"));
        employeeList.add(new Employee("Test", 1, "Senior"));
        employeeList.add(new Employee("Test", 2, "Senior"));

        Mockito.when(employeeService.getEmployee(1L)).thenReturn(employeeList.get(0));
        Mockito.when(financeServiceClient.getSalary("Junior", 1)).thenReturn(100);

        Mockito.when(employeeService.getEmployee(3L)).thenReturn(employeeList.get(2));
        Mockito.when(financeServiceClient.getSalary("Mid", 1)).thenReturn(300);

        Mockito.when(employeeService.getEmployee(6L)).thenReturn(employeeList.get(5));
        Mockito.when(financeServiceClient.getSalary("Senior", 1)).thenReturn(600);


        mockMvc.perform(MockMvcRequestBuilders.get("/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(100));

        mockMvc.perform(MockMvcRequestBuilders.get("/3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(300));

        mockMvc.perform(MockMvcRequestBuilders.get("/6"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(600));
    }
}
