package com.ikea.inter.hrservice.service;

import com.ikea.inter.hrservice.data.EmployeeRepository;
import com.ikea.inter.hrservice.model.Employee;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class EmployeeServiceTest {

    @MockBean
    private EmployeeRepository repository;

    @Autowired
    EmployeeService employeeService;

    @Test
    void getEmployee_test(){
        Employee junior = new Employee("Test", 1, "Junior");
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(junior));

        Employee mid = new Employee("Test", 1, "Mid");
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(mid));

        Employee senior = new Employee("Test", 1, "Senior");
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(senior));

        Employee result = employeeService.getEmployee(1L);
        Assert.assertEquals(result.getName(), "Test");
        Assert.assertEquals(result.getLevel(), "Junior");
        Assert.assertEquals((int)result.getTime(),1);
        Assert.assertSame(result, junior);

        Employee resultMid =employeeService.getEmployee(2L);
        Assert.assertSame(resultMid, mid);

        Employee resultSenior =employeeService.getEmployee(3L);
        Assert.assertSame(resultSenior, senior);

    }
}
