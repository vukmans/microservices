package com.ikea.inter.hrservice.data;

import com.ikea.inter.hrservice.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(path = "employee")
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
