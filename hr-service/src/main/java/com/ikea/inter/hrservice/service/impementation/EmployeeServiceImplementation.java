package com.ikea.inter.hrservice.service.impementation;

import com.ikea.inter.hrservice.data.EmployeeRepository;
import com.ikea.inter.hrservice.model.Employee;
import com.ikea.inter.hrservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class EmployeeServiceImplementation implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImplementation(EmployeeRepository employeeRepository){
        this.employeeRepository=employeeRepository;
    }

    @Override
    public Employee getEmployee(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        return employee.get();
    }
}
