package com.ikea.inter.hrservice.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
//@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    @Column(name = "employee_id")
    private Long id;

    @Column(name = "employee_name")
    private String name;

    @Column(name = "employee_time")
    private Integer time;

    @Column(name = "employee_level")
    private String level;

    public Employee(String name, Integer time, String level) {
        this.name = name;
        this.time = time;
        this.level = level;
    }
}
