package com.ikea.inter.hrservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("finance-service")
public interface FinanceServiceClient {

    @GetMapping("/salaries")
    Integer getSalary(@RequestParam("level") String level, @RequestParam("time") Integer time);
}

