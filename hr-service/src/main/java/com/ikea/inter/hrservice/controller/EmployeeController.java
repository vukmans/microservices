package com.ikea.inter.hrservice.controller;


import com.ikea.inter.hrservice.model.Employee;
import com.ikea.inter.hrservice.model.Salary;
import com.ikea.inter.hrservice.service.EmployeeService;
import com.ikea.inter.hrservice.service.FinanceServiceClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
public class EmployeeController {

    private final EmployeeService employeeService;
    private final FinanceServiceClient financeServiceClient;

    @Autowired
    public EmployeeController(EmployeeService employeeService, FinanceServiceClient financeServiceClient){
        this.employeeService=employeeService;
        this.financeServiceClient=financeServiceClient;
    }

    @GetMapping(value= "/{id}", produces = APPLICATION_JSON_VALUE)
    //@HystrixCommand(fallbackMethod = "fallback")
    public Salary calculateSalary(@PathVariable Long id){
        log.info("Getting employee information...");
        Employee employee = employeeService.getEmployee(id);
        log.info("Level: {} Time: {}", employee.getLevel(), employee.getTime());
        Integer salary = financeServiceClient.getSalary(employee.getLevel(), employee.getTime());
        return new Salary(salary);
    }

    //public Salary fallback(){ return new Salary(-100);}

}
