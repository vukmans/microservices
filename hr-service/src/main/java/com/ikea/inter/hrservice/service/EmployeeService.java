package com.ikea.inter.hrservice.service;

import com.ikea.inter.hrservice.model.Employee;

public interface EmployeeService {

    public Employee getEmployee(Long id);
}
